import com.atlassian.bamboo.specs.api.BambooSpec
import com.atlassian.bamboo.specs.api.builders.BambooKey
import com.atlassian.bamboo.specs.api.builders.plan.Job
import com.atlassian.bamboo.specs.api.builders.plan.Plan
import com.atlassian.bamboo.specs.api.builders.plan.Stage
import com.atlassian.bamboo.specs.api.builders.plan.artifact.Artifact
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement
import com.atlassian.bamboo.specs.api.builders.project.Project
import com.atlassian.bamboo.specs.builders.task.CheckoutItem
import com.atlassian.bamboo.specs.builders.task.ScriptTask
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask
import com.atlassian.bamboo.specs.builders.trigger.RepositoryPollingTrigger
import com.atlassian.bamboo.specs.util.BambooServer
import com.atlassian.bamboo.specs.util.SimpleUserPasswordCredentials

@BambooSpec
class PlanSpec {
    static void main(String... args) {
        //By default credentials are read from the '.credentials' file.
        def creds = new SimpleUserPasswordCredentials(args[0], args[1])
        BambooServer bambooServer = new BambooServer("http://localhost:8080", creds)

        Plan rootObject = new Plan(
            new Project()
                .key(new BambooKey("PROJ1")), "Narcinator", new BambooKey("NARC")
            )
            .description("Illustrates static build analysis")
            .stages(
                new Stage("Default Stage")
                    .jobs(
                        new Job("Default Job",
                        new BambooKey("JOB1"))
                            .artifacts(new Artifact()
                            .name("Static analysis")
                            .copyPattern("**/*.html")
                            .location("build/reports/codenarc/")
                            .shared(false))
                        .tasks(
                            new VcsCheckoutTask()
                                .description("Checkout Default Repository")
                                .checkoutItems(new CheckoutItem().defaultRepository()),
                                new ScriptTask()
                                    .description("Run Static analysis")
                                    .inlineBody("./gradlew check")
                        )
                    )
            )
            .linkedRepositories("Codenarc Example")
            .triggers(new RepositoryPollingTrigger()
            .name("Repository polling"))
            .planBranchManagement(
                new PlanBranchManagement()
                    .delete(new BranchCleanup())
                    .notificationForCommitters()
            )

        bambooServer.publish(rootObject)
    }
}