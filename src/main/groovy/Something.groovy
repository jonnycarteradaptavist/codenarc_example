class Something {
    public static final someprop = "sample"

    def someMethod(foo, bar, bin, bash) {
        "$foo, $bar, $bin, $bash"
    }
}